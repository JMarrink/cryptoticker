function getValue()
	{
     var result = null;
     var scriptUrl = "script/weather.php";
     $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
     });
     return result;
}

function setWeather() {
    var data = getValue();

    $( ".themp" ).empty();
    $( ".themp" ).append(data['themp']+'&deg;');

    $(".icon").attr("src", 'img/icons/'+data['icon']+'.png');
}
