var coins = ["dash", "litecoin", "ripple"];

function showGetResult(name)
	{
     var result = null;
     var scriptUrl = "script/coininfo.php?function=currency&currency="+name+"";
     $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
     });
     return result;
}


function fillHtml(data) {
	var	coinHtml = '<div class="coin '+data['name']+'"><h2>'+data['name']+'</h2> <div class="percentage '+data['movement']+'">'+data['percent_change_24h']+' % <i class="fa '+data['icon']+'" aria-hidden="true"></i></div><div class="amount">€ '+data['price_eur']+'</div></div>';
	$( '.cryptoticker' ).append(coinHtml);
}

function setData(name) {
	fillHtml(showGetResult(name));
}

function coinUpdate(name) {
	data = showGetResult(name);
    var change = data['percent_change_24h']+' %';
	$( '.cryptoticker' ).find("."+data['name']).find( '.percentage' ).removeClass('up down').addClass(data['movement']);
	$( '.cryptoticker' ).find("."+data['name']).find( '.percentage' ).text(change);
	$( '.cryptoticker' ).find("."+data['name']).find( '.percentage' ).append(' <i class="fa '+data['icon']+'" aria-hidden="true"></i>');
	$( '.cryptoticker' ).find("."+data['name']).find( '.amount' ).text("€ "+data['price_eur']);
}
