function getInternet()
	{
     var result = null;
     var scriptUrl = "script/internet.php";
     $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
     });
     return result;
}

function getInternetStatus()
    {
        if (getInternet() == true) {
            $( '.internet' ).empty();
            $( '.internet' ).removeClass("down");
            $( '.internet' ).addClass('up');
            $( '.internet' ).append('<img src="img/icons/up.png" alt="">');
        } else {
            $( '.internet' ).empty();

            if ( $( '.internet' ).hasClass( "down" ) ) {
                $( '.internet' ).removeClass("up");
            }   else {
                $( '.internet' ).addClass('down');
                var audio = new Audio('js/nuke.mp3');
                audio.play();
            }
            $( '.internet' ).append('<img src="img/icons/down.png" alt="">');
        }
    }
