function showValue()
	{
     var result = null;
     var scriptUrl = "script/coininfo.php?function=coinvalue";
     $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
     });
     return result;
}


function inputHtml(data) {
    $.each( data, function( key, value ) {

        var name = key;
        var coins = value;
        var coinhtml = [];
        var count = 0;

        $.each( coins, function( coinname, coinvalue ) {

            coinhtml[count] = '<li><div class="coinname">'+coinname+'</div><div class="coinvalue">€ <span class="coinamount">'+coinvalue+'</span></div></li>';
            // console.log(coinhtml);
            count++;
        });

        var basehtml = '<ul class="user"><li><h3>'+name+'</h3><ul class="coins '+name+'"></ul></li></ul>'

        $( '.walletlist' ).append(basehtml);

        $.each( coinhtml, function( id , content ) {
            $( '.'+name+'' ).append(content);
        });
        // var allhtml = '<ul><li><h3>'+name+'</h3><ul>'+coinhtml+'</ul></li></ul>';
        // console.log(coinhtml);
        calcValue(name, coins);
    });
}

function getSum(total, num) {
    return total + num;
}

function calcValue(name, coins) {
    // console.log(coins);
    var total = []
    var count = 0;

    var totalamount = [];

    $.each( coins, function( id , content ) {
        totalamount[count] = parseInt(content);
        count++;
    });

    $( '.'+name+'' ).append('<li class="total">€ '+totalamount.reduce(getSum)+'</li>');

}


function coinValue() {
    inputHtml(showValue());
    console.log(showValue());
}

function showWallet() {
    $( ".wallets" ).show();
}

function hideWallet() {
    $( ".wallets" ).hide();
}
