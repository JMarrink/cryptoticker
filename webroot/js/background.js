function getImgUrl() {
    var result = null;
    var scriptUrl = "script/background.php";
    $.ajax({
        url: scriptUrl,
        type: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });
    return result;
}

function changeImg() {
    var html = '<div class="background hide"></div>';

    $('.background').after(html);

    $(".hide").css('background-image', 'url(' + getImgUrl() + ')');
    $(".background:first").delay(5000).fadeOut('slow', function() {
        $(".hide").fadeIn('slow', function() {
            $(".background:first").remove();
            $(".hide").removeClass("hide");

        });
    });
}
