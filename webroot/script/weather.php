

<?php
function getData()
{
    $raw_data = file_get_contents('http://api.openweathermap.org/data/2.5/weather?q=groningen,nl&units=metric&appid=12d0690727f2775ad62f54e3375e6195');
    $data = json_decode($raw_data, true);

    return $data;
}

$weatherdata = array();

function sortData($data)
{
    $weatherdata['themp'] = round($data['main']['temp'], 0, PHP_ROUND_HALF_DOWN);
    $weatherdata['icon'] = $data['weather'][0]['icon'];

    return($weatherdata);
}



$rawdata = getData();
$returndata = sortData($rawdata);

echo json_encode($returndata);
