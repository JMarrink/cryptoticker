<?php

$folder = "../img";
$content = array_diff(scandir($folder), array('..', '.'));

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "dashboard";


// foreach ($content as $key => $value) {
//     $variable = substr($value, 0, strpos($value, "."));
//     $stmt->execute();
//
//
//     echo "New records created successfully";
//
//     $stmt->close();
//     $conn->close();
// }
//
// $sql = new PDO('mysql:host=localhost;dbname=dashboard', $username, $password);
//
// $sql = "INSERT INTO backgrounds(filename)";
//
// $stmt = $pdo->prepare($sql);
//
// $stmt->bindParam(':filename', 'test', PDO::PARAM_STR);
//
// $stmt->execute();

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO backgrounds (file)
    VALUES (:file)");
    $stmt->bindParam(':file', $file);

    foreach ($content as $key => $value) {
        // insert a row
        $file = $value;
        $stmt->execute();
    }

    echo "New records created successfully";
}

catch(PDOException $e)
    {
    echo "Error: " . $e->getMessage();
    }
$conn = null;
