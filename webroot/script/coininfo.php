<?php

function getCurrency($currency)
{
    $data = getData($currency);
    $movement = upDown($data[0]['percent_change_24h']);
    $icon = getIcon($movement);

    $list = array(
        'name' => $data[0]['name'],
        'percent_change_24h' => $data[0]['percent_change_24h'],
        'price_eur' => round($data[0]['price_eur'], 2),
        'movement' => $movement,
        'icon' => $icon
        );

    return $list;
}

function getData($currency)
{
    $raw_data = file_get_contents('https://api.coinmarketcap.com/v1/ticker/'.$currency.'/?convert=EUR');
    $data = json_decode($raw_data, true);

    return $data;
}

function getIcon($updown)
{
    if ($updown === "up") {
        return "fa-angle-up";
    } else {
        return "fa-angle-down";
    }
}

function upDown($percentchange)
{
    if (strpos($percentchange, '-') !== false) {
        return "down";
    } else {
        return "up";
    }
}

function coinValue()
{
    $database = array(
        "Marijn" => array("dash" => 0.194259),
        "Jens" => array("dash" => 0.239687, "litecoin" => 0.66066144),
        "Rohan" => array("ripple" => 250)
    );

    $result = array();

    foreach ($database as $person => $value) {
        $coins = array();

        foreach ($value as $coinname => $coin) {
            $data = getData($coinname);
            $price_eur = (float)$data[0]['price_eur'];
            $percentage = $coin * 100/1;

            $coinvalue = $price_eur / 100 * $percentage;

            $coins[$coinname] =  str_replace('.', ',', number_format($coinvalue, 2));
        }
        $result[$person] = $coins;
    }
    return $result;
}





switch ($_GET['function']) {
    case "currency":
        $returndata = getCurrency($_GET["currency"]);

        echo json_encode($returndata);
        break;
    case "coinvalue":
         $returndata = coinValue();

         echo json_encode($returndata);
        break;
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}
