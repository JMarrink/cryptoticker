<?php

// $dsn = 'mysql:dbname=dashboard;host=localhost';
// $user = 'root';
// $password = 'root';
// $number = mt_rand(2, 171);
//
// $dbh = new PDO($dsn, $user, $password);
// $stmt = $dbh->prepare("SELECT file FROM backgrounds WHERE id=$number LIMIT 1");
// $stmt->execute();
// $row = $stmt->fetch();


function get_headers_from_curl_response($response)
{
    $headers = array();

    $header_text = substr($response, 0, strpos($response, "\r\n\r\n"));

    foreach (explode("\r\n", $header_text) as $i => $line)
        if ($i === 0)
            $headers['http_code'] = $line;
        else
        {
            list ($key, $value) = explode(': ', $line);

            $headers[$key] = $value;
        }

    return $headers;
}

 // create curl resource
        $ch = curl_init();
        // set url
        curl_setopt($ch, CURLOPT_URL, "https://source.unsplash.com/random/1920x1080");
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);



        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        // echo "<pre>";
        $headers = get_headers_from_curl_response($output);
        // var_dump($headers['Location']);


echo json_encode($headers['Location']);
