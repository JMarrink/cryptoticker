<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>test</title>
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">

        <style media="screen">

            body {
                margin: 0;
                background-color: #000;
            }

            .background {
                height: 100vh;
                background-size: cover;
            }

            .hide {
                display: none;
            }

            .overlay {
                position: absolute;
                top: 0;
                height: 100vh;
                width: 100vw;
                color: #FFF;
                font-family: 'Roboto', sans-serif;
            }

            .time {
                position: absolute;
                right: 20px;
                font-size: 110px;
            }

            .cryptoticker {
                position: absolute;
                bottom: 0px;
                right: 8px;
            }

            .coin {
                display: inline;
                float: left;
                margin-right: 22px;
                text-align: center;
            }

            .coin h2 {
                float: left;
                margin-top: 5px;
            }

            .percentage {
                float: left;
                display: inline;
                line-height: 45px;
                margin-left: 10px;
            }

            .amount {
                display: none;
            }

            .weather {
                position: absolute;
                bottom: 0;
                right: 298px;
            }

            .themp {
                float: left;
                font-size: 47px;
                line-height: 78px;
            }

            .weather img {
                max-width: 66px;
                float: left;
                margin-left: 11px;

            }

            .target {
                color: #FFFFFF;
                width: 464px;
                height: 125px;
                position: absolute;
                bottom: 100px;
                border-radius: 7px;
                /* background-color: #00000045; */
                right: 100px;
                text-shadow: 1px 1px 2px #000;
                -webkit-transition: width 1s; /* For Safari 3.1 to 6.0 */
                transition: width 1s;
            }

            .internet img {
                max-width: 70px;
                position: absolute;
                top: 50px;
                left: 50px;
                display: none;
            }

            .wallets {
                position: absolute;
                right: 20px;
                top: 5px;
                display: none;
            }
            .walletlist ul {
                padding: 0;
            }

            .walletlist {
                text-align: center;
            }

            ul.walletlist li {
                list-style: none;
            }

            ul.user {
                float: left;
                background: #00000045;
                min-height: 150px;
                padding: 15px;
                position: relative;
            }
            .total {
                border-top: 1px solid;
                /* margin: 0px -10px; */
                position: absolute;
                bottom: 8px;
                width: 100%;
                right: 0;
                font-size: 21px;
                padding-top: 10px;
            }

            .coinname {
                text-transform: capitalize;
            }

            h3 {
                margin-top: 0px;
                font-size: 28px;
                text-transform: capitalize;
                margin-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <div class="background"></div>
        <div class="overlay">
            <div class="internet">

            </div>
            <div class="wallets">
                <ul class="walletlist">

                </ul>
            </div>
            <div class="target">
                <div class="weather">
                    <div class="themp">

                    </div>
                    <img class="icon" src="" alt="">
                </div>
                <div class="time ">
                    <div id="clock"></div>
                </div>
            </div>

        </div>
    </body>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/internet.js"></script>
    <script src="js/youtube.js"></script>
    <script src="js/weather.js"></script>
    <script src="js/coinvalue.js"></script>
    <script src="js/cryptoticker.js"></script>
    <script src="js/background.js"></script>
    <script src="js/functions.js"></script>
</html>
