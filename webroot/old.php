<!DOCTYPE html>
<html lang="nl">
<head>
	<meta charset="UTF-8">
	<title>fullscreen</title>


	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	<style>
		body {
			margin: 0;
			background: #000;
			font-family: 'Roboto', sans-serif;
		}

		.coin {
			float: left;
    		background: #ffffff95;
    		padding: 14px;
		}

		.coin h2 {
			font-size: 30px;
		}

		.coin * {
			display: inline;;
		}

		.coin .amount {
			display: block;
		}

		.coin .up {
			color: #008000;
		}

		.coin .down {
			color: #ff0000
		}

		.percentage {
			font-weight: 700;
			font-size: 25px
		}

		.amount {
			font-size: 20px;
		}

		.coin .fa {
			font-size: 28px;
		}

		.cryptoticker {
		    position: absolute;
    		bottom: 20px;
    		left: 20px;
    		cursor: none;
		}

		.overlay {
			z-index: 99;
			position: absolute;
			height: 100vh;
			width: 100%;
		}

		.walletlist ul {
    		padding: 0;
		}

		.walletlist {
			text-align: center;
		}

		ul.walletlist li {
			list-style: none;
		}

		ul.user {
			float: left;
		    background: #ffffff95;
		    min-height: 200px;
		    padding: 15px;
		    position: relative;
		}

		h3 {
			margin-top: 0px;
		    font-size: 28px;
		    text-transform: capitalize;
		    margin-bottom: 10px;
		}

		.coinname {
			text-transform: capitalize;
			font-size: 24px;
			font-weight: 700;
		}

		.coinvalue {
			font-size: 20px;
		}

		ul.coins li {
		    margin-bottom: 10px;
		}

		.total {
			border-top: 1px solid;
		    /* margin: 0px -10px; */
		    position: absolute;
		    bottom: 0;
		    width: 100%;
		    right: 0;
		    font-size: 21px;
    		padding-top: 10px;
		}

		.wallets {
			position: absolute;
            right: 20px;
            top: 5px;
		}

		.music {
		    position: absolute;
    		top: 20px;
    		left: 20px;
		}

		.embed-container {
			display: none;
		}

		.music section {
			float: left;
    		background: #ffffff95;
    		padding: 15px;
		}

		section a {
		    font-size: 30px;
    		color: #000;
		}

		.fa-play {
			margin-right: 5px;
		}

		section .active {
			color: #6f6f6f;
		}

	.time {
	    position: absolute;
    	z-index: 99;
    	top: 20px;
    	right: 20px;
    	background: #ffffff95;
    	padding: 15px;
	}

	#clock {
	    font-size: 50px;
    	font-family: 'Roboto', sans-serif;
	}
	</style>
</head>

<body>
		<div class="time">
			<div id="clock"></div>
		</div>
	<div class="overlay">
		<div class="music">
			<section>
  <a href="javascript:void callPlayer(&quot;videoembed1&quot;,&quot;playVideo&quot;)"><i class="fa fa-play" aria-hidden="true"></i></a>
  <a href="javascript:void callPlayer(&quot;videoembed1&quot;,&quot;pauseVideo&quot;)"><i class="fa fa-pause" aria-hidden="true"></i></a>

  <div id="videoembed1" class="embed-container">
    <iframe width="640" height="390" frameborder="0" title="YouTube video player" type="text/html" src="https://www.youtube.com/embed/zcUUZ1R5ZwU?feature=oembed&controls=0&hd=1&modestbranding=1&autohide=1&showinfo=0&enablejsapi=1"></iframe>
  </div>
</section>
		</div>
		<div class="wallets">
			<ul class="walletlist">

			</ul>
		</div>
		<div class="cryptoticker"></div>
	</div>

<div class="flx-embed"><div style="position:relative; height:0; padding-bottom:56.25%; width:100%"><iframe id="fullscreen" style="position:absolute; top:0; left:0; width:100%; height:100%;" src="https://media.flixel.com/cinemagraph/rykqpfk72605o0t2xqki?hd=true" frameborder="0" allowfullscreen></iframe></div></div>
</body>


<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="js/youtube.js"></script>
<script src="js/coinvalue.js"></script>
<script src="js/cryptoticker.js"></script>
<script src="js/background.js"></script>
<script src="js/functions.js"></script>
</html>
